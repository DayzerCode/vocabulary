# Приложение для пополнения английского словарного запаса 
Основные функции: добавление слов и их перевод.Перевод слов выполняется запросом на сервер,
сами слова (исходные и переведенные) хранятся локально с помощью Realm. 
После добавления можно начать закрепления слов, также имеется возможность настройки закрепления.

Приложение написано на Java, используемый стек: MVP, Realm, RxJava2 Retrofit, OkHttp

##### Скриншоты:
![alt text](https://bitbucket.org/DayzerCode/vocabulary/raw/5a6ed8b9b26e5a77d695d923ed77c5e44f065282/screenshots/translate.png) ![alt text](https://bitbucket.org/DayzerCode/vocabulary/raw/5a6ed8b9b26e5a77d695d923ed77c5e44f065282/screenshots/list.png)
![alt text](https://bitbucket.org/DayzerCode/vocabulary/raw/5a6ed8b9b26e5a77d695d923ed77c5e44f065282/screenshots/proccess.png) ![alt text](https://bitbucket.org/DayzerCode/vocabulary/raw/5a6ed8b9b26e5a77d695d923ed77c5e44f065282/screenshots/statistics.png) 
![alt text](https://bitbucket.org/DayzerCode/vocabulary/raw/5a6ed8b9b26e5a77d695d923ed77c5e44f065282/screenshots/settings.png) ![alt text](https://bitbucket.org/DayzerCode/vocabulary/raw/5a6ed8b9b26e5a77d695d923ed77c5e44f065282/screenshots/menu.png)