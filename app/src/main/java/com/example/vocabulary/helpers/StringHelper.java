package com.example.vocabulary.helpers;

public class StringHelper {
    public static String repeat(String string, Integer count) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < count ; i++) {
            result.append(string);
        }
        return result.toString();
    }
}
