package com.example.vocabulary.helpers;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHelper {
    public static String getFormatDate(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return simpleDateFormat.format(date);
    }
}
