package com.example.vocabulary.helpers;

import android.content.Context;

import io.realm.Realm;
import io.realm.exceptions.RealmMigrationNeededException;

public class RealmHelper {
    public static Realm getInstance(Context context) {
        Realm realm;
        Realm.init(context);
        try {
            realm = Realm.getDefaultInstance();
        } catch (RealmMigrationNeededException e) {
            Realm.removeDefaultConfiguration();
            realm = Realm.getDefaultInstance();
        }
        return realm;
    }
}
