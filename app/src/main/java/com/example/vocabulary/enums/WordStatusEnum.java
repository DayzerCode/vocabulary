package com.example.vocabulary.enums;

public class WordStatusEnum {
    final public static int NOT_LEARNED = 0;
    final public static int ALMOST_LEARNED = 5;
    final public static int LEARNED = 10;

    public static String getText(int statusId) {
        String statusText;
        switch (statusId) {
            case WordStatusEnum.NOT_LEARNED:
                statusText = "не выучено";
                break;
            case WordStatusEnum.ALMOST_LEARNED:
                statusText = "почти выучено";
                break;
            case WordStatusEnum.LEARNED:
                statusText = "выучено";
                break;
            default:
                statusText = "неизвестно";
        }
        return statusText;
    }
}
