package com.example.vocabulary.method_translate;

import androidx.annotation.Nullable;

import com.example.vocabulary.contracts.method_translate.LangToLang;
import com.example.vocabulary.enums.MethodTranslateEnum;
import com.example.vocabulary.helpers.StringHelper;
import com.example.vocabulary.models.realm.Word;
import com.example.vocabulary.settings.SettingFixingWords;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * an instance of a class alternately returns words that can be translated. The translation method (from Russian into English or from English into Russian) is taken from the application settings
 */
public class MethodTranslate {
    private List<Word> listWords;
    private int currentIndex = 0;
    private LangToLang methodTranslate;

    public MethodTranslate(List<Word> listWords, SettingFixingWords setting) {
        List<Word> newList = new ArrayList<>(listWords);
        Collections.shuffle(newList);
        this.listWords = newList;

        if (setting.getMethodTranslate() == MethodTranslateEnum.RU_TO_EN) {
            methodTranslate = new RussianToEnglish();
        } else {
            methodTranslate = new EnglishToRussian();
        }
        if (!isWordsEnded()) {
            methodTranslate.setWord(getCurrentWord());
        }
    }

    /**
     * Method use for recovery state
     *
     * @param index saved index before destroy activity
     */
    public void setCurrentIndex(int index) {
        currentIndex = index;
        methodTranslate.setWord(getCurrentWord());
    }

    /**
     * increase in inner index by 1, further, calling getCurrentWord() will return the next word in the list
     */
    public void nextWord() {
        currentIndex++;
        if (!isWordsEnded()) {
            methodTranslate.setWord(getCurrentWord());
        }
    }

    public boolean isWordsEnded() {
        return currentIndex >= listWords.size();
    }

    @Nullable
    public Word getCurrentWord() {
        if (!isWordsEnded()) {
            return listWords.get(currentIndex);
        }
        return null;
    }

    public boolean isTranslationCorrect(String receivedWord) {
        return methodTranslate.getTranslatedWord().toLowerCase().equals(receivedWord.toLowerCase());
    }

    public String getWordNeedTranslate() {
        return methodTranslate.getWordNeedTranslate();
    }

    public int getCurrentIndex() {
        return currentIndex;
    }

    public String getTooltip() {
        String translatedWord = methodTranslate.getTranslatedWord();
        int halfWordCountSymbol = translatedWord.length() / 2;
        int remainderDivision = (translatedWord.length() % 2 == 0 ? 0 : 1);
        return translatedWord.substring(0, halfWordCountSymbol) + StringHelper.repeat("*", halfWordCountSymbol + remainderDivision);
    }
}
