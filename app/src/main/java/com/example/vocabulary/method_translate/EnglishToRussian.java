package com.example.vocabulary.method_translate;

import com.example.vocabulary.contracts.method_translate.LangToLang;
import com.example.vocabulary.models.realm.Word;

public class EnglishToRussian implements LangToLang {
    private Word word;

    @Override
    public void setWord(Word word) {
        this.word = word;
    }

    public String getWordNeedTranslate() {
        return word.getEnglishValue();
    }

    public String getTranslatedWord() {
        return word.getRussianValue();
    }
}
