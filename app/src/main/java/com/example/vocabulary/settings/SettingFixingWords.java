package com.example.vocabulary.settings;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.vocabulary.enums.MethodTranslateEnum;
import com.example.vocabulary.enums.WordStatusEnum;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class SettingFixingWords {
    private SharedPreferences parameters;
    private static String SETTING_NAME = "fixing_words_setting";
    public SettingFixingWords(Context context) {
        parameters = context.getSharedPreferences(SETTING_NAME, Context.MODE_PRIVATE);
    }

    public void putMethodTranslate(int  methodTranslate) {
        parameters.edit().putInt("method_translate", methodTranslate).apply();
    }
    public int getMethodTranslate() {
        return parameters.getInt( "method_translate", MethodTranslateEnum.EN_TO_RU);
    }

    public void putStatuses(HashSet<String> statuses) {
        parameters.edit().putStringSet("statuses", statuses).apply();
    }

    public List<Integer> getStatuses() {
        HashSet<String> hashSetStatuses =  (HashSet<String>) parameters.getStringSet("statuses", getStatusesDefValue());
        List<Integer> listStatuses = new ArrayList<>();
        for (String status: hashSetStatuses) {
            listStatuses.add(Integer.valueOf(status));
        }
        return listStatuses;
    }

    HashSet<String> getStatusesDefValue() {
        HashSet<String> hashSetStatuses = new HashSet<>();
        hashSetStatuses.add(String.valueOf(WordStatusEnum.NOT_LEARNED));
        return hashSetStatuses;
    }
}
