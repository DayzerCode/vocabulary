package com.example.vocabulary.contracts.ui.setting_fixing_words;

import android.view.View;

public interface SettingFixingView {
    View getRoot();
}
