package com.example.vocabulary.contracts.ui.list_words;

import com.example.vocabulary.models.realm.Word;

import java.util.List;

public interface ListWordsView {
    void initRecyclerView(List<Word> listWords);
}
