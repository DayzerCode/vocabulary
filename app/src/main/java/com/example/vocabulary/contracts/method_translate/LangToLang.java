package com.example.vocabulary.contracts.method_translate;

import com.example.vocabulary.models.realm.Word;

public interface LangToLang {
    void setWord(Word word);
    String getWordNeedTranslate();
    String getTranslatedWord();
}
