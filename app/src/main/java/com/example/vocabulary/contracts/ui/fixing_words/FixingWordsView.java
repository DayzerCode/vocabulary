package com.example.vocabulary.contracts.ui.fixing_words;

import android.view.View;

public interface FixingWordsView {
    View getRoot();

    void showSuccessButtons();

    void hideSuccessButtons();

    void changeEnteringWord(String string);

    void wordsEnded();

    void clearEnteredWord();

    void showFailTranslate();

    void showTooltip(String text);

    void hideTooltip();
}
