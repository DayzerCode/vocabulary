package com.example.vocabulary.contracts.ui.random_list_words;

import com.example.vocabulary.network.responses.WordResponse;

import java.util.List;

public interface RandomListWordsView {
    void onDestroy();

    void showError(String error);

    void initRecyclerView(List<WordResponse> listWords);

    void randomListLoading();

    void randomListLoaded();
}
