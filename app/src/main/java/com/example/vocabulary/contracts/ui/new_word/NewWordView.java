package com.example.vocabulary.contracts.ui.new_word;

public interface NewWordView {

    void notifyWhenWordAdded(String successText);

    void setTranslatedWord(String word);

    void showError(String error);

}
