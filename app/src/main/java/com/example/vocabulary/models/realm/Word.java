package com.example.vocabulary.models.realm;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Required;

public class Word extends RealmObject {
    @Required
    private String id;
    @Required
    private String englishValue;
    @Required
    private String russianValue;

    private int status;
    @Required
    private Date createdDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEnglishValue() {
        return englishValue;
    }

    public void setEnglishValue(String englishValue) {
        this.englishValue = englishValue;
    }

    public String getRussianValue() {
        return russianValue;
    }

    public void setRussianValue(String russianValue) {
        this.russianValue = russianValue;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}

