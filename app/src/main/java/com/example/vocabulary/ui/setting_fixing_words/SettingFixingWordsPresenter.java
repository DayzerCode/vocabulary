package com.example.vocabulary.ui.setting_fixing_words;

import androidx.annotation.Nullable;

import com.example.vocabulary.contracts.ui.setting_fixing_words.SettingFixingView;
import com.example.vocabulary.enums.WordStatusEnum;
import com.example.vocabulary.settings.SettingFixingWords;

import java.util.HashSet;

public class SettingFixingWordsPresenter {
    @Nullable
    private SettingFixingView view;
    private SettingFixingWords parameters;
    SettingFixingWordsPresenter(SettingFixingView view) {
        this.view = view;
        parameters = new SettingFixingWords(view.getRoot().getContext());
    }

    public void detachView () {
        view = null;
    }

    SettingFixingWords getParameters() {
        return parameters;
    }

    void saveStatuses(boolean notLearnedStatusOption, boolean almostLearnedStatusOption, boolean learnedStatusOption) {
        HashSet<String> statuses = new HashSet<>();
        if (notLearnedStatusOption) {
            statuses.add(String.valueOf(WordStatusEnum.NOT_LEARNED));
        }
        if (almostLearnedStatusOption) {
            statuses.add(String.valueOf(WordStatusEnum.ALMOST_LEARNED));
        }
        if (learnedStatusOption) {
            statuses.add(String.valueOf(WordStatusEnum.LEARNED));
        }
        parameters.putStatuses(statuses);
    }

    void saveMethodTranslate(int status) {
        parameters.putMethodTranslate(status);
    }
}
