package com.example.vocabulary.ui.list_words;

import com.example.vocabulary.models.realm.Word;
import com.example.vocabulary.repositories.WordRepository;

import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.realm.Realm;
import io.realm.RealmResults;

public class ListWordsPresenter {
    private ListWordsFragment view;
    private Realm realm;
    private WordRepository wordRepository;

    private CompositeDisposable disposable = new CompositeDisposable();

    ListWordsPresenter(ListWordsFragment view, Realm realm, WordRepository wordRepository) {
        this.view = view;
        this.realm = realm;
        this.wordRepository = wordRepository;
    }

    public void detachView() {
        realm.close();
        disposable.dispose();
        view = null;
    }

    public void onRemoveWord(String wordId) {
        wordRepository.remove(wordId);
    }

    Flowable<RealmResults<Word>> searchByDict(String searchWord) {
        return (searchWord.equals("") ? wordRepository.getAllAsFlowable() : wordRepository.searchAsFlowable(searchWord));
    }
}
