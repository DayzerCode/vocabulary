package com.example.vocabulary.ui.home;

import com.example.vocabulary.Dto.StatisticsWordsDto;
import com.example.vocabulary.enums.WordStatusEnum;
import com.example.vocabulary.repositories.WordRepository;

import io.realm.Realm;

public class HomePresenter {
    private HomeFragment view;
    private Realm realm;
    private WordRepository wordRepository;

    HomePresenter(HomeFragment view, Realm realm, WordRepository wordRepository) {
        this.view = view;
        this.realm = realm;
        this.wordRepository = wordRepository;
    }

    void onInit() {
        StatisticsWordsDto statisticsWordsDto = new StatisticsWordsDto();
        statisticsWordsDto.all = wordRepository.getCountWords();
        statisticsWordsDto.notLearned = wordRepository.getCountWords(WordStatusEnum.NOT_LEARNED);
        statisticsWordsDto.almostLearned = wordRepository.getCountWords(WordStatusEnum.ALMOST_LEARNED);
        statisticsWordsDto.learned = wordRepository.getCountWords(WordStatusEnum.LEARNED);
        view.fillStatistics(statisticsWordsDto);
    }

    public void detachView() {
        realm.close();
        view = null;
    }
}
