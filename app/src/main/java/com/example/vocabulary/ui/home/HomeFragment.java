package com.example.vocabulary.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.vocabulary.Dto.StatisticsWordsDto;
import com.example.vocabulary.R;
import com.example.vocabulary.helpers.RealmHelper;
import com.example.vocabulary.repositories.WordRepository;

import io.realm.Realm;

public class HomeFragment extends Fragment {
    private TextView textViewAllWords;
    private TextView textViewNotLearnedWords;
    private TextView textAlmostLearnedWords;
    private TextView textLearnedWords;
    private HomePresenter presenter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        textViewAllWords = root.findViewById(R.id.textViewHomeFragmentAllWords);
        textViewNotLearnedWords = root.findViewById(R.id.textViewHomeFragmentNotLearnedWords);
        textAlmostLearnedWords = root.findViewById(R.id.textViewHomeFragmentAlmostLearnedWords);
        textLearnedWords = root.findViewById(R.id.textViewHomeFragmentLearnedWords);
        Realm realm = RealmHelper.getInstance(this.getContext());
        WordRepository wordRepository = new WordRepository(realm);
        presenter = new HomePresenter(this, realm, wordRepository);
        presenter.onInit();
        return root;
    }

    void fillStatistics(StatisticsWordsDto statisticsWordsDto) {
        textViewAllWords.setText(String.valueOf(statisticsWordsDto.all));
        textViewNotLearnedWords.setText(String.valueOf(statisticsWordsDto.notLearned));
        textAlmostLearnedWords.setText(String.valueOf(statisticsWordsDto.almostLearned));
        textLearnedWords.setText(String.valueOf(statisticsWordsDto.learned));
    }

    @Override
    public void onDestroy() {
        if (presenter != null) {
            presenter.detachView();
        }
        super.onDestroy();
    }
}
