package com.example.vocabulary.ui.list_words.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vocabulary.R;

class ListWordsViewHolder extends RecyclerView.ViewHolder {
    final TextView textViewListWordsEnglishWord;
    final TextView textViewListWordsRussianWord;
    final TextView textViewListWordsStatus;
    final TextView textViewDateAdded;
    final ImageView imageViewListWordsDelete;
    ListWordsViewHolder(@NonNull View itemView) {
        super(itemView);
        textViewListWordsEnglishWord = itemView.findViewById(R.id.textViewListWordsEnglishWord);
        textViewListWordsRussianWord = itemView.findViewById(R.id.textViewListWordsRussianWord);
        textViewListWordsStatus = itemView.findViewById(R.id.textViewListWordsStatus);
        textViewDateAdded = itemView.findViewById(R.id.textViewListWordsDateAdded);
        imageViewListWordsDelete = itemView.findViewById(R.id.imageViewListWordsDelete);
    }
}
