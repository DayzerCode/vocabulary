package com.example.vocabulary.ui.random_list_words.adpters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vocabulary.R;
import com.example.vocabulary.network.responses.WordResponse;
import com.example.vocabulary.ui.random_list_words.RandomListWordsPresenter;

import java.util.List;

public class RandomListWordsAdapter extends RecyclerView.Adapter<RandomListWordsViewHolder> {
    private LayoutInflater inflater;
    private List<WordResponse> listWords;
    private RandomListWordsPresenter presenter;

    public RandomListWordsAdapter(Context context, List<WordResponse> listWords, RandomListWordsPresenter presenter) {
        this.inflater = LayoutInflater.from(context);
        this.listWords = listWords;
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public RandomListWordsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.random_word_in_list, parent, false);
        return new RandomListWordsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RandomListWordsViewHolder holder, final int position) {
        final WordResponse word = listWords.get(position);
        holder.tvRussianWord.setText(word.getRussian());
        holder.tvEnglishWord.setText(word.getEnglish());
        holder.ivAddToDict.setOnClickListener(v -> presenter.onAddToDict(word));
    }

    @Override
    public int getItemCount() {
        return listWords.size();
    }
}
