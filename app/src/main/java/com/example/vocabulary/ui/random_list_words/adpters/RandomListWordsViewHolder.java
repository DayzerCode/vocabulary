package com.example.vocabulary.ui.random_list_words.adpters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vocabulary.R;

class RandomListWordsViewHolder extends RecyclerView.ViewHolder {
    final TextView tvRussianWord;
    final TextView tvEnglishWord;
    final ImageView ivAddToDict;
    RandomListWordsViewHolder(@NonNull View itemView) {
        super(itemView);
        tvRussianWord = itemView.findViewById(R.id.tvRussianWord);
        tvEnglishWord = itemView.findViewById(R.id.tvEnglishWord);
        ivAddToDict = itemView.findViewById(R.id.ivAddToDict);
    }
}
