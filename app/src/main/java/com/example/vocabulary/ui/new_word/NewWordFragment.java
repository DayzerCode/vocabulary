package com.example.vocabulary.ui.new_word;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.vocabulary.R;
import com.example.vocabulary.contracts.ui.new_word.NewWordView;
import com.example.vocabulary.helpers.RealmHelper;
import com.example.vocabulary.network.NetworkService;
import com.example.vocabulary.repositories.WordRepository;

import io.realm.Realm;

public class NewWordFragment extends Fragment implements NewWordView {
    private View root;
    private Button buttonTranslate;
    private EditText editTextInputWord;
    private TextView textViewTranslationWordResult;
    private TextView textViewSourceWord;
    private TextView textViewCancelLoading;
    private LinearLayout linearLayoutLoading;
    private Button buttonNewWordSave;
    private Resources resources;
    private NewWordPresenter presenter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        // init
        this.root = inflater.inflate(R.layout.fragment_new_word, container, false);
        initWidgets();
        Realm realm = RealmHelper.getInstance(root.getContext());
        WordRepository wordRepository = new WordRepository(realm);
        NetworkService networkService = NetworkService.getInstance();
        resources = root.getContext().getResources();
        presenter = new NewWordPresenter(this, realm, wordRepository, networkService, resources);

        buttonTranslate.setOnClickListener(v -> presenter.onTranslate(
                editTextInputWord.getText().toString()
            )
        );

        buttonNewWordSave.setOnClickListener(v ->
                presenter.onSaveNewWord(
                        textViewSourceWord.getText().toString(),
                        textViewTranslationWordResult.getText().toString()
                )
        );

        textViewCancelLoading.setOnClickListener(v -> presenter.onCancelLoadingTranslationWord());
        return root;
    }

    public void notifyWhenWordAdded(String successText) {
        editTextInputWord.setText("");
        textViewTranslationWordResult.setText("");
        textViewSourceWord.setText("");
        buttonNewWordSave.setEnabled(false);
        Toast.makeText(root.getContext(), successText, Toast.LENGTH_SHORT).show();
    }

    public void setTranslatedWord(String word) {
        textViewTranslationWordResult.setText(word);
        buttonNewWordSave.setEnabled(true);
    }

    public void showError(String error) {
        Toast.makeText(root.getContext(), error, Toast.LENGTH_SHORT).show();
    }

    void translationWordLoading() {
        linearLayoutLoading.setVisibility(View.VISIBLE);
    }

    void translationWordLoaded() {
        linearLayoutLoading.setVisibility(View.GONE);
    }

    private void initWidgets() {
        buttonTranslate = root.findViewById(R.id.buttonFragmentNewWordTranslate);
        editTextInputWord = root.findViewById(R.id.editTextFragmentNewWordInputWord);
        textViewTranslationWordResult = root.findViewById(R.id.textViewFragmentTranslationWordResult);
        textViewSourceWord = root.findViewById(R.id.textViewFragmentNewWordSourceWord);
        buttonNewWordSave = root.findViewById(R.id.buttonFragmentNewWordSave);
        linearLayoutLoading = root.findViewById(R.id.linearLayoutFragmentNewWordLoading);
        textViewCancelLoading = root.findViewById(R.id.textViewFragmentNewWordCancelLoading);
    }

    @Override
    public void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    void copySourceWord() {
        textViewSourceWord.setText(editTextInputWord.getText().toString());
    }
}
