package com.example.vocabulary.ui.fixing_words;

import android.content.res.Resources;

import com.example.vocabulary.method_translate.MethodTranslate;
import com.example.vocabulary.models.realm.Word;

import io.realm.Realm;

class FixingWordsPresenter {
    private FixingWordsFragment view;
    private Realm realm;
    private MethodTranslate methodTranslate;
    private Resources resources;

    FixingWordsPresenter(FixingWordsFragment view, Realm realm, MethodTranslate methodTranslate, Resources resources) {
        this.view = view;
        this.realm = realm;
        this.methodTranslate = methodTranslate;
        this.resources = resources;
    }

    void onSaveAsLearned(int learnedStatus) {
        if (methodTranslate.getCurrentWord() != null) {
            realm.beginTransaction();
            Word word = methodTranslate.getCurrentWord();
            word.setStatus(learnedStatus);
            realm.commitTransaction();
        }
        onNextWord();
    }

    int getCurrentIndex() {
        return methodTranslate.getCurrentIndex();
    }

    void setCurrentIndex(int index) {
        methodTranslate.setCurrentIndex(index);
    }

    void detachView () {
        realm.close();
        view = null;
    }

    void onSetEnteringWord() {
        if (!methodTranslate.isWordsEnded()) {
            view.changeEnteringWord(methodTranslate.getWordNeedTranslate());
        } else {
            view.wordsEnded();
        }

    }

    void onWordTranslated(String string) {
        if (methodTranslate.isTranslationCorrect(string)) {
            view.showSuccessButtons();
        } else {
            view.showFailTranslate();
        }
    }

    void onNextWord() {
        if (methodTranslate.isWordsEnded()) {
            view.wordsEnded();
        } else {
            clearLastWord();
            methodTranslate.nextWord();
            onSetEnteringWord();
        }
    }

    void onShowTooltip() {
        view.showTooltip(methodTranslate.getTooltip());
    }

    private void clearLastWord() {
        view.hideSuccessButtons();
        view.clearEnteredWord();
        view.hideTooltip();
    }
}
