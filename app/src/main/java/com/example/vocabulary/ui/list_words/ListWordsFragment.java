package com.example.vocabulary.ui.list_words;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vocabulary.R;
import com.example.vocabulary.contracts.ui.list_words.ListWordsView;
import com.example.vocabulary.models.realm.Word;
import com.example.vocabulary.repositories.WordRepository;
import com.example.vocabulary.ui.list_words.adapters.ListWordsAdapter;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.BackpressureStrategy;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.realm.Realm;

public class ListWordsFragment extends Fragment implements ListWordsView {
    private RecyclerView recyclerViewListWords;
    private EditText editTextSearch;
    private ListWordsPresenter presenter;
    private Disposable disposable;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list_words, container, false);
        recyclerViewListWords = root.findViewById(R.id.recyclerViewFragmentListWords);
        editTextSearch = root.findViewById(R.id.editTextFragmentListWordsSearch);
        Realm realm = Realm.getDefaultInstance();
        WordRepository wordRepository = new WordRepository(realm);
        presenter = new ListWordsPresenter(this, realm, wordRepository);
        textSearchListen();
        return root;
    }

    public void initRecyclerView(List<Word> listWords) {
        ListWordsAdapter adapter = new ListWordsAdapter(getContext(), listWords, presenter);
        recyclerViewListWords.setAdapter(adapter);
    }

    private void textSearchListen() {
        disposable = RxTextView.textChangeEvents(editTextSearch)
                .debounce(300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .toFlowable(BackpressureStrategy.BUFFER)
                .switchMap(textChangeEvent -> presenter.searchByDict(editTextSearch.getText().toString()))
                .subscribe(this::initRecyclerView);
    }


    @Override
    public void onPause() {
        super.onPause();
        disposable.dispose();
    }

    @Override
    public void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }
}
