package com.example.vocabulary.ui.new_word;

import android.content.res.Resources;

import com.example.vocabulary.R;
import com.example.vocabulary.models.realm.Word;
import com.example.vocabulary.network.NetworkService;
import com.example.vocabulary.network.responses.TranslationResponse;
import com.example.vocabulary.repositories.WordRepository;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;

class NewWordPresenter {
    private NewWordFragment view;
    private Realm realm;
    private WordRepository wordRepository;
    private NetworkService networkService;
    private Resources resources;
    private CompositeDisposable disposable = new CompositeDisposable();

    NewWordPresenter(NewWordFragment view, Realm realm, WordRepository wordRepository, NetworkService networkService, Resources resources) {
        this.view = view;
        this.realm = realm;
        this.wordRepository = wordRepository;
        this.networkService = networkService;
        this.resources = resources;
    }

    void detachView() {
        realm.close();
        disposable.clear();
        view = null;
    }

    void onTranslate(String inputWord) {
        if (inputWord.isEmpty()) {
            view.showError(resources.getString(R.string.fragment_new_word_error_empty_input));
            return;
        }
        view.translationWordLoading();
        disposable.add(networkService.getJsonHolderApi().translateFromRussianToEnglish(inputWord)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handleError));
    }

    private void handleResponse(TranslationResponse translationResponse) {
        view.translationWordLoaded();
        if (translationResponse == null) {
            view.showError(resources.getString(R.string.server_response_error));
            return;
        }
        if (!translationResponse.getStatus().equals("success")) {
            // show error from server
            view.showError(translationResponse.getText());
            return;
        }
        // for success response
        view.setTranslatedWord(translationResponse.getText());
        view.copySourceWord();
    }

    private void handleError(Throwable throwable) {
        view.showError(resources.getString(R.string.network_error_throwable));
    }

    void onSaveNewWord(String inputWord, String translationWordResult) {
        if (inputWord.isEmpty() && translationWordResult.isEmpty()) {
            view.showError(resources.getString(R.string.fragment_new_word_error_empty_values));
            return;
        }
        realm.beginTransaction();
        Word word = wordRepository.make();
        word.setRussianValue(inputWord);
        word.setEnglishValue(translationWordResult);
        realm.commitTransaction();
        view.notifyWhenWordAdded(resources.getString(R.string.fragment_new_word_success_added_word));
    }

    void onCancelLoadingTranslationWord() {
        disposable.dispose();
        view.translationWordLoaded();
    }
}
