package com.example.vocabulary.ui.random_list_words;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vocabulary.R;
import com.example.vocabulary.contracts.ui.random_list_words.RandomListWordsView;
import com.example.vocabulary.helpers.RealmHelper;
import com.example.vocabulary.network.NetworkService;
import com.example.vocabulary.network.responses.WordResponse;
import com.example.vocabulary.repositories.WordRepository;
import com.example.vocabulary.ui.random_list_words.adpters.RandomListWordsAdapter;

import java.util.List;

import io.realm.Realm;

public class RandomListWordsFragment extends Fragment implements RandomListWordsView {
    private View root;
    private Button btnRefresh;
    private RecyclerView rvRandomListWords;
    private LinearLayout llLoading;
    private RandomListWordsPresenter presenter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        // init
        this.root = inflater.inflate(R.layout.fragment_random_list_words, container, false);
        initWidgets();
        Realm realm = RealmHelper.getInstance(root.getContext());
        WordRepository wordRepository = new WordRepository(realm);
        NetworkService networkService = NetworkService.getInstance();
        Resources resources = root.getContext().getResources();
        presenter = new RandomListWordsPresenter(this, realm, wordRepository, networkService, resources);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.onRefreshRandomList();
        btnRefresh.setOnClickListener(v -> presenter.onRefreshRandomList());
    }

    @Override
    public void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    public void showError(String error) {
        Toast.makeText(root.getContext(), error, Toast.LENGTH_SHORT).show();
    }

    public void initRecyclerView(List<WordResponse> listWords) {
        RandomListWordsAdapter adapter = new RandomListWordsAdapter(getContext(), listWords, presenter);
        rvRandomListWords.setAdapter(adapter);
    }

    public void randomListLoading() {
        llLoading.setVisibility(View.VISIBLE);
        rvRandomListWords.setVisibility(View.GONE);
    }

    public void randomListLoaded() {
        llLoading.setVisibility(View.GONE);
        rvRandomListWords.setVisibility(View.VISIBLE);
    }

    private void initWidgets() {
        btnRefresh = root.findViewById(R.id.btnRefresh);
        rvRandomListWords = root.findViewById(R.id.rvRandomListWords);
        llLoading = root.findViewById(R.id.llLoading);
    }
}
