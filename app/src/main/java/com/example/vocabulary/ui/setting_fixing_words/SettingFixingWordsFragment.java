package com.example.vocabulary.ui.setting_fixing_words;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.vocabulary.R;
import com.example.vocabulary.contracts.ui.setting_fixing_words.SettingFixingView;
import com.example.vocabulary.enums.WordStatusEnum;

public class SettingFixingWordsFragment extends Fragment implements SettingFixingView {

    private SettingFixingWordsPresenter presenter;
    private View root;

    private CheckBox notLearnedStatusOption;
    private CheckBox almostLearnedStatusOption;
    private CheckBox learnedStatusOption;
    private Spinner methodTranslate;
    private Button buttonSave;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        this.root = inflater.inflate(R.layout.fragment_setting_fixing_words, container, false);
        presenter = new SettingFixingWordsPresenter(this);
        initWidgets();
        prepareSettingView();
        buttonSave.setOnClickListener(v -> save());
        return this.root;
    }

    @Override
    public void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    private void save() {
        presenter.saveStatuses(notLearnedStatusOption.isChecked(), almostLearnedStatusOption.isChecked(), learnedStatusOption.isChecked());
        presenter.saveMethodTranslate(methodTranslate.getSelectedItemPosition());
    }

    public View getRoot() {
        return root;
    }

    private void prepareSettingView() {
        for (int status: presenter.getParameters().getStatuses()) {
            switch (status) {
                case WordStatusEnum.NOT_LEARNED:
                    notLearnedStatusOption.setChecked(true);
                    break;
                case WordStatusEnum.ALMOST_LEARNED:
                    almostLearnedStatusOption.setChecked(true);
                    break;
                case WordStatusEnum.LEARNED:
                    learnedStatusOption.setChecked(true);
                    break;
            }
        }
        methodTranslate.setSelection(presenter.getParameters().getMethodTranslate());
    }
    private void initWidgets() {
        notLearnedStatusOption = root.findViewById(R.id.checkboxFragmentSettingFixingWordsNotLearnedStatusOption);
        almostLearnedStatusOption = root.findViewById(R.id.checkboxFragmentSettingFixingWordsAlmostLearnedStatusOption);
        learnedStatusOption = root.findViewById(R.id.checkboxFragmentSettingFixingWordsLearnedStatusOption);
        methodTranslate = root.findViewById(R.id.spinnerFragmentSettingFixingWordsMethodTranslate);
        buttonSave = root.findViewById(R.id.buttonFragmentSettingFixingWordsSave);
    }
}
