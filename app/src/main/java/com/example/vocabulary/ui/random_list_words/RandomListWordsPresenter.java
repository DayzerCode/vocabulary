package com.example.vocabulary.ui.random_list_words;

import android.content.res.Resources;

import com.example.vocabulary.R;
import com.example.vocabulary.network.NetworkService;
import com.example.vocabulary.network.responses.RandomWordsResponse;
import com.example.vocabulary.network.responses.WordResponse;
import com.example.vocabulary.repositories.WordRepository;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;

public class RandomListWordsPresenter {
    private RandomListWordsFragment view;
    private Realm realm;
    private WordRepository wordRepository;
    private NetworkService networkService;
    private Resources resources;
    private CompositeDisposable disposable = new CompositeDisposable();

    RandomListWordsPresenter(RandomListWordsFragment view, Realm realm, WordRepository wordRepository, NetworkService networkService, Resources resources) {
        this.view = view;
        this.realm = realm;
        this.wordRepository = wordRepository;
        this.networkService = networkService;
        this.resources = resources;
    }

    void detachView() {
        realm.close();
        disposable.clear();
        view = null;
    }

    void onRefreshRandomList() {
        view.randomListLoading();
        disposable.add(networkService.getJsonHolderApi().getRandomWords()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handleError));
    }

    private void handleResponse(RandomWordsResponse response) {
        view.randomListLoaded();
        if (response == null || response.getWords().size() <= 0) {
            view.showError(resources.getString(R.string.server_response_error));
            return;
        }
        view.initRecyclerView(response.getWords());
    }

    private void handleError(Throwable throwable) {
        view.showError(resources.getString(R.string.network_error_throwable));
    }


    public void onAddToDict(WordResponse word) {

    }
}
