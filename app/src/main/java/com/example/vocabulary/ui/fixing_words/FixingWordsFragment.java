package com.example.vocabulary.ui.fixing_words;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.vocabulary.R;
import com.example.vocabulary.contracts.ui.fixing_words.FixingWordsView;
import com.example.vocabulary.enums.WordStatusEnum;
import com.example.vocabulary.helpers.RealmHelper;
import com.example.vocabulary.method_translate.MethodTranslate;
import com.example.vocabulary.models.realm.Word;
import com.example.vocabulary.repositories.WordRepository;
import com.example.vocabulary.settings.SettingFixingWords;

import java.util.List;

import io.realm.Realm;

public class FixingWordsFragment extends Fragment implements FixingWordsView {
    private View root;
    private TextView WordNeedTranslate;
    private EditText editTextEnteredWord;
    private Button buttonTranslateWord;
    private Button buttonSkip;
    private Button buttonCanStillLearn;
    private Button buttonMarkAsLearned;
    private Button buttonShowTooltip;
    private TextView textViewTooltip;
    private TextView textViewStatusBar;
    private LinearLayout linearLayoutLearnButtons;
    private Resources resources;
    private FixingWordsPresenter presenter;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);
        root = inflater.inflate(R.layout.fragment_fixing_words, container, false);
        initWidgets();

        resources = root.getContext().getResources();
        Realm realm = RealmHelper.getInstance(root.getContext());
        WordRepository wordRepository = new WordRepository(realm);
        SettingFixingWords setting = new SettingFixingWords(root.getContext());
        List<Word> listWords = wordRepository.getAllByStatuses(setting.getStatuses());
        MethodTranslate methodTranslate = new MethodTranslate(listWords, setting);
        presenter = new FixingWordsPresenter(this, realm, methodTranslate, resources);
        if (savedInstanceState != null) {
            presenter.setCurrentIndex(savedInstanceState.getInt("currentIndex"));
        }

        presenter.onSetEnteringWord();
        buttonTranslateWord.setOnClickListener(v -> presenter.onWordTranslated(editTextEnteredWord.getText().toString()));
        buttonSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onNextWord();
            }
        });

        buttonCanStillLearn.setOnClickListener(v -> presenter.onSaveAsLearned(WordStatusEnum.ALMOST_LEARNED));

        buttonMarkAsLearned.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onSaveAsLearned(WordStatusEnum.LEARNED);
            }
        });

        buttonShowTooltip.setOnClickListener(v -> presenter.onShowTooltip());

        return root;
    }

    @Override
    public void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putInt("currentIndex", presenter.getCurrentIndex());
        super.onSaveInstanceState(outState);
    }

    public View getRoot() {
        return root;
    }

    public void showSuccessButtons() {
        linearLayoutLearnButtons.setVisibility(View.VISIBLE);

        buttonTranslateWord.setVisibility(View.GONE);
        buttonShowTooltip.setVisibility(View.GONE);
        hideTooltip();

        showSuccessTranslate();
    }

    public void hideSuccessButtons() {
        linearLayoutLearnButtons.setVisibility(View.GONE);

        buttonTranslateWord.setVisibility(View.VISIBLE);
        buttonShowTooltip.setVisibility(View.VISIBLE);

        clearStatusBar();
    }

    public void changeEnteringWord(String string) {
        WordNeedTranslate.setText(string);
    }

    public void wordsEnded() {
        Toast.makeText(root.getContext(), resources.getString(R.string.fragment_fixing_words_words_ended), Toast.LENGTH_SHORT).show();
        WordNeedTranslate.setText(resources.getString(R.string.fragment_fixing_words_input_unavailable));
        buttonTranslateWord.setEnabled(false);
        buttonSkip.setEnabled(false);
        buttonShowTooltip.setVisibility(View.GONE);
    }

    public void clearEnteredWord() {
        editTextEnteredWord.setText("");
    }

    public void showFailTranslate() {
        textViewStatusBar.setTextColor(resources.getColor(R.color.colorError));
        textViewStatusBar.setText(resources.getString(R.string.fragment_fixing_words_incorrect_translation));
    }

    private void showSuccessTranslate() {
        textViewStatusBar.setTextColor(resources.getColor(R.color.colorSuccess));
        textViewStatusBar.setText(resources.getString(R.string.fragment_fixing_words_success));
    }

    private void clearStatusBar() {
        textViewStatusBar.setText("");
    }

    public void showTooltip(String text) {
        textViewTooltip.setText(text);
    }

    public void hideTooltip() {
        textViewTooltip.setText("");
    }

    private void initWidgets() {
        WordNeedTranslate = root.findViewById(R.id.textViewFragmentFixingWordsWordNeedTranslate);
        editTextEnteredWord = root.findViewById(R.id.editTextFragmentFixingWordsInputTranslateWord);
        buttonTranslateWord = root.findViewById(R.id.buttonFragmentFixingWordsTranslateWord);
        buttonSkip = root.findViewById(R.id.buttonFragmentFixingWordsSkip);
        buttonCanStillLearn = root.findViewById(R.id.buttonFragmentFixingWordsCanStillLearn);
        buttonMarkAsLearned = root.findViewById(R.id.buttonFragmentFixingWordsMarkAsLearned);
        buttonShowTooltip = root.findViewById(R.id.buttonFragmentFixingWordsShowTooltip);
        textViewTooltip = root.findViewById(R.id.textViewFragmentFixingWordsTooltip);
        textViewStatusBar = root.findViewById(R.id.textViewFragmentFixingWordsStatusBar);
        linearLayoutLearnButtons = root.findViewById(R.id.linearLayoutFragmentFixingWordsLearnButtons);
    }
}
