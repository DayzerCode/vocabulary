package com.example.vocabulary.ui.list_words.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vocabulary.R;
import com.example.vocabulary.enums.WordStatusEnum;
import com.example.vocabulary.helpers.DateHelper;
import com.example.vocabulary.models.realm.Word;
import com.example.vocabulary.ui.list_words.ListWordsPresenter;

import java.util.List;

public class ListWordsAdapter extends RecyclerView.Adapter<ListWordsViewHolder> {
    private LayoutInflater inflater;
    private List<Word> listWords;
    private ListWordsPresenter presenter;

    public ListWordsAdapter(Context context, List<Word> listWords, ListWordsPresenter presenter) {
        this.inflater = LayoutInflater.from(context);
        this.listWords = listWords;
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public ListWordsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_words, parent, false);
        return new ListWordsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ListWordsViewHolder holder, final int position) {
        final Word word = listWords.get(position);
        holder.textViewListWordsEnglishWord.setText(word.getEnglishValue());
        holder.textViewListWordsRussianWord.setText(word.getRussianValue());
        String statusText = WordStatusEnum.getText(word.getStatus());
        holder.textViewListWordsStatus.setText(statusText);

        holder.textViewDateAdded.setText(DateHelper.getFormatDate(word.getCreatedDate()));
        holder.imageViewListWordsDelete.setOnClickListener(v -> {
            presenter.onRemoveWord(word.getId());
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return listWords.size();
    }
}
