package com.example.vocabulary.repositories;

import com.example.vocabulary.enums.WordStatusEnum;
import com.example.vocabulary.models.realm.Word;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import io.reactivex.Flowable;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

public class WordRepository {
    private Realm realm;

    public WordRepository(Realm realm) {
        this.realm = realm;
    }

    public Word make() {
        Word word = realm.createObject(Word.class);
        word.setId(UUID.randomUUID().toString());
        word.setCreatedDate(new Date());
        word.setStatus(WordStatusEnum.NOT_LEARNED);
        return word;
    }

    public List<Word> getAll() {
        RealmResults<Word> result = realm.where(Word.class).findAll();
        result.sort("createdDate", Sort.DESCENDING);
        return result;
    }

    public Flowable<RealmResults<Word>> getAllAsFlowable() {
        return realm.where(Word.class).findAllAsync().sort("createdDate", Sort.DESCENDING).asFlowable();
    }


    public Flowable<RealmResults<Word>> searchAsFlowable(String word) {
        RealmResults<Word> result = realm.where(Word.class).contains("russianValue", word).or().contains("englishValue", word).findAllAsync();
        result.sort("createdDate", Sort.DESCENDING);
        return result.asFlowable();
    }

    public List<Word> getAllByStatuses(List<Integer> statuses) {
        RealmQuery<Word> query = realm.where(Word.class).equalTo("status", statuses.get(0));
        if (statuses.size() > 1) {
            for (int i = 1; i < statuses.size() ; i++) {
                query.or().equalTo("status", statuses.get(i));
            }
        }
        return query.findAll();
    }

    public void remove(String id) {
        realm.beginTransaction();
        realm.where(Word.class).equalTo("id", id).findFirst().deleteFromRealm();
        realm.commitTransaction();
    }

    public long getCountWords() {
        return realm.where(Word.class).count();
    }

    public long getCountWords(int status) {
        return realm.where(Word.class).equalTo("status", status).count();
    }
}
