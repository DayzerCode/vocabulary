package com.example.vocabulary.network.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WordResponse {
    @SerializedName("english")
    @Expose
    private String english;
    @SerializedName("russian")
    @Expose
    private String russian;

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getRussian() {
        return russian;
    }

    public void setRussian(String russian) {
        this.russian = russian;
    }
}
