package com.example.vocabulary.network;

import com.example.vocabulary.network.responses.RandomWordsResponse;
import com.example.vocabulary.network.responses.TranslationResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface JsonHolderApi {
    @GET("translate/")
    Single<TranslationResponse> translateFromRussianToEnglish(@Query("word") String word);

    @GET("random-words/")
    Single<RandomWordsResponse> getRandomWords();
}
