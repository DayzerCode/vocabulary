package com.example.vocabulary.network.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RandomWordsResponse {
    @SerializedName("words")
    @Expose
    private List<WordResponse> words;

    public List<WordResponse> getWords() {
        return words;
    }

    public void setWords(List<WordResponse> words) {
        this.words = words;
    }
}
