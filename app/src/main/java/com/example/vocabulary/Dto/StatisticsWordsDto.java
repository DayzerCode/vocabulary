package com.example.vocabulary.Dto;

public class StatisticsWordsDto {
    public long all;
    public long notLearned;
    public long almostLearned;
    public long learned;
}
